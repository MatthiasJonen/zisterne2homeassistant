# zisterne2homeassistant

## Description
Simple mqtt datastream reading the serial output of a custom water filling measurement device.

## Installation
- Setup the raspberry pi zero. Here is [some explanation](https://iot4beginners.com/how-to-read-and-write-from-serial-port-using-raspberry-pi/). Main step for me: enable the serial interface using\
```sudo raspi-config```
- Change into the /opt directory and clone this repo with\
```sudo git clone https://gitlab.com/MatthiasJonen/zisterne2homeassistant.git```
- Create .env file as shown in .env.Example
- Execute the install script with\
```/opt/zisterne2homeassistant/install.sh```

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Open an issue in this repo.

## Roadmap
I don't have plans to further add features. It works for me.

## Authors and acknowledgment
[Matthias Jonen](https://gitlab.com/MatthiasJonen)

## License
MIT

## Project status
Done and happy with it.
