#!/usr/bin/bash

sudo apt install -y python3-serial python3-paho-mqtt python3-dotenv
sudo git pull
sudo ln -sf $PWD/zisterne2homeassistant.service /etc/systemd/system/zisterne2homeassistant.service
sudo systemctl daemon-reload
sudo systemctl enable zisterne2homeassistant.service
sudo systemctl start zisterne2homeassistant.service
sudo systemctl status zisterne2homeassistant.service
