#!/usr/bin/python


import serial
import re
import paho.mqtt.client as paho
from dotenv import dotenv_values

config = dotenv_values(".env",)

ser = serial.Serial(
  port="/dev/ttyS0",
  baudrate = 9600,
  parity=serial.PARITY_NONE,
  stopbits=serial.STOPBITS_ONE,
  bytesize=serial.EIGHTBITS,
  timeout=1,
)

while True:
  measurement = str(ser.readline())
  if measurement != "b''":
    # Extract filling percentage out of measurement string
    filling_percentage_incl_unit = "".join(re.findall("[0-9]* %",measurement))
    filling_percentage = filling_percentage_incl_unit.replace(" %","")
    # Check if filling percentage is in reasonable range.
    # Then publish filling percentage using mqtt
    try:
      if 0 <= int(filling_percentage) <= 110:
        print(filling_percentage)
        client = paho.Client()
        client.username_pw_set(config["MQTT_USERNAME"], config["MQTT_PASSWORD"])
        if client.connect(config["MQTT_BROKER"], 1883, 60) != 0:
            print("Could not connect to MQTT Broker!")
            sys.exit(-1)
        client.publish(config["MQTT_TOPIC"], filling_percentage)
        client.disconnect()
    except:
      print("Error, in variable filling_percentage:" + filling_percentage)
